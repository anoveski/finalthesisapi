var express = require('express');
var router = express.Router();
var User = require('../../models/user-module/user');
var privateUserController = require('../../controllers/private-user-login/private-user-login');
var businessUserController = require('../../controllers/business-user-login/business-user-login');

router.post('/login', (req, res) => {
    User.findOne({
        email: req.body.email
    }, function (err, user) {
        if (err) throw err;

        if (!user) {
            res.status(400).send({
                success: false, errorLocation: {
                    emailError: true,
                    passwordError: false
                }, message: 'User not found.'
            });
        } else {
            if (user.role === 'BUSINESS_USER') {
                businessUserController({
                    email: req.body.email,
                    password: req.body.password
                }, function (result) {
                    res.status(result.code).send(result.response);
                })
            } else if (user.role === 'PRIVATE_USER') {
                privateUserController({
                    email: req.body.email,
                    password: req.body.password
                }, function (result) {
                    res.status(result.code).send(result.response);
                })
            }
        }
    });
});

module.exports = router;
