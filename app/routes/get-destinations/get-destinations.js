var express = require('express');
var router = express.Router();
var DestinationModel = require('../../models/destination-model/destination');

router.get('/getAvailableDestinations', (req, res) => {
    DestinationModel('StartLocation', function (locationSchema) {
        locationSchema.find({}, function (err, startLocations) {
            if (err) throw err;
            if (startLocations) {
                DestinationModel('DestinationLocations', function (locationSchema) {
                    locationSchema.find({}, function (err, endLocations) {
                        if (err) throw err;
                        if (endLocations) {
                            res.status(200).json({
                                startLocations: startLocations,
                                endLocations: endLocations
                            });
                        }
                    })
                });
            }
        })
    });
});

module.exports = router;
