var express = require('express');
var router = express.Router();
var passport = require('passport');
var jwt = require('jwt-simple');
var date = require('date-and-time');
var getToken = require('../../controllers/get-token/get-token');
var PurchasedTicket = require('../../models/purchased-ticket-model/purchased-ticket');

router.post('/getPurchasedTickets', passport.authenticate('jwt', {session: false}), (req, res) => {
    var token = getToken(req.headers);

    var userProfile = jwt.decode(token, process.env.APPLICATION_SECRET);

    var todayDate = (date.format(new Date((new Date()).valueOf() - 1000 * 3600 * 24), 'YYYY-MM-DD') + 'T22:00:00.000Z');

    var condition;

    if (userProfile.role === 'PRIVATE_USER') {
        if (req.body.typeOfTickets === 'active_purchased_all') {
            condition = {
                customerId: userProfile._id,
                expireDate: {$gte: todayDate}
            };
        } else if (req.body.typeOfTickets === 'today_active_tickets') {
            condition = {
                customerId: userProfile._id,
                '$or': [{
                    'selectedDeparture.startLocationDate': todayDate
                }, {
                    'selectedDeparture.returnLocationDate': todayDate
                }]
            };
        } else if (req.body.typeOfTickets === 'inactive_tickets') {
            condition = {
                customerId: userProfile._id,
                expireDate: {$lt: todayDate}
            };
        }
    } else if (userProfile.role === 'BUSINESS_USER') {
        if (req.body.typeOfTickets === 'active_purchased_all') {
            condition = {
                companyId: userProfile._id,
                expireDate: {$gte: todayDate}
            }
        } else if (req.body.typeOfTickets === 'today_active_tickets') {
            condition = {
                companyId: userProfile._id,
                '$or': [{
                    'selectedDeparture.startLocationDate': todayDate
                }, {
                    'selectedDeparture.returnLocationDate': todayDate
                }]
            }
        } else if (req.body.typeOfTickets === 'inactive_tickets') {
            condition = {
                companyId: userProfile._id,
                expireDate: {$lt: todayDate}
            };
        }
    }

    PurchasedTicket.find(condition, function (err, tickets) {
        if (err) throw err;
        if (!tickets) {
            res.status(200).json({tickets: 0, message: 'You haven`t bought any tickets'});
        } else {
            res.status(200).json({tickets: tickets});
        }
    })
});

module.exports = router;
