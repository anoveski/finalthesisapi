var express = require('express');
var router = express.Router();
var passport = require('passport');
var generator = require('generate-serial-number');
var Ticket = require('../../models/ticket-model/ticket');
var getBusinessUserProfile = require('../../controllers/get-business-user-profile/get-business-user-profile');
var addNewDestination = require('../../controllers/destination/destination');
var getToken = require('../../controllers/get-token/get-token');

router.post('/addTicket', passport.authenticate('jwt', {session: false}), (req, res) => {
    var token = getToken(req.headers);

    getBusinessUserProfile(token, function (user) {

        var newTicket = new Ticket({
            companyNo: user.companyNo,
            companyId: user._id,
            companyName: user.companyName,
            companyPhoneNumber: user.companyPhoneNumber,
            ticketSerialNo: generator.generate(7),
            validityStatus: 1,
            startLocation: {
                townOrMunicipality: req.body.startLocation.townOrMunicipality,
                country: req.body.startLocation.country
            },
            destinationLocation: {
                townOrMunicipality: req.body.destinationLocation.townOrMunicipality,
                country: req.body.destinationLocation.country
            },
            typeOfTransportationVehicle: req.body.typeOfTransportationVehicle,
            availableSeats: req.body.availableSeats,
            oneWayPrice: req.body.oneWayPrice,
            returnTicketPrice: req.body.returnTicketPrice,
            oneWayTicket: req.body.oneWayTicket,
            returnTicket: req.body.returnTicket,
            typeOfRelation: req.body.typeOfRelation,
            relationData: {
                weeklyRelation: req.body.relationData.weeklyRelation,
                everyDayRelation: req.body.relationData.everyDayRelation,
                dailyRelation: req.body.relationData.dailyRelation
            },
            expireDate: extractTicketExpireDate(req.body.typeOfRelation, req.body.relationData)
        });

        newTicket.save(function (err) {
            if (err) {
                res.status(501).json({success: false, message: 'Ticket not added'})
            } else {

                res.status(201).json({success: true, message: 'Successful added ticket!'});

                addNewDestination('StartLocation', req.body.startLocation, function (status) {
                });
                addNewDestination('DestinationLocations', req.body.destinationLocation, function (status) {
                })
            }
        })
    });

    function extractTicketExpireDate(typeOfRelation, ticketDates) {
        if (typeOfRelation === 'select_days') {
            var [lastSelectedDayDate] = ticketDates.dailyRelation.slice(-1);

            if (lastSelectedDayDate.dayDate !== '') {
                return lastSelectedDayDate.dayDate;
            }
        } else if (typeOfRelation === 'weekly' && ticketDates.weeklyRelation.repeatEveryWeek === false) {
            var [lastWeeklyDate] = ticketDates.weeklyRelation.selectedWeek.slice(-1);

            if (lastWeeklyDate.endDate !== '') {
                return lastWeeklyDate.endDate;
            } else {
                if (lastWeeklyDate.startDate !== '') {
                    return lastWeeklyDate.startDate;
                } else {
                    return null;
                }
            }
        } else {
            return '0';
        }
    }
});

module.exports = router;
