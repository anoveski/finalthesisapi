var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PurchasedTicketSchema = new Schema({
    expireDate: {
        type: String,
        required: true
    },
    purchasedTicketNo: {
        type: Number,
        required: true
    },
    companyId: {
        type: String,
        required: true
    },
    companyNo: {
        type: String,
        required: true
    },
    companyName: {
        type: String,
        required: true
    },
    startLocation: {
        townOrMunicipality: {
            type: String,
            required: true
        },
        country: {
            name: {
                type: String,
                required: true
            },
            code: {
                type: String,
                required: true
            }
        },
        type: Object,
        required: true
    },
    destinationLocation: {
        endTownOrMunicipality: {
            type: String,
            required: true
        },
        country: {
            name: {
                type: String,
                required: true
            },
            code: {
                type: String,
                required: true
            }
        },
        type: Object,
        required: true
    },
    customerId: {
        type: String,
        required: true
    },
    activePurchasedTicketStatus: {
        type: Boolean,
        required: true
    },
    todayActivePurchasedTicketStatus: {
        type: Boolean,
        required: true
    },
    ticketId: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    ticketType: {
        type: String,
        required: true
    },
    passenger: {
        firstName: {
            type: String,
            required: true
        },
        lastName: {
            type: String,
            required: true
        },
        email: {
            type: String,
            required: true
        }
    },
    selectedDeparture: {
        startLocationDate: {
            type: String,
            required: true
        },
        startLocationHour: {
            departureTime: {
                type: String,
                required: true
            }
        },
        returnLocationDate: {
            type: String,
        },
        returnLocationHour: {
            departureTime: {
                type: String,
            }
        }
    }
});

module.exports = mongoose.model('PurchasedTicket', PurchasedTicketSchema);
