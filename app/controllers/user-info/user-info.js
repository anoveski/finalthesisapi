var User = require('../../models/user-module/user');
var jwt = require('jwt-simple');

module.exports = function (token, next) {
    if (token) {
        var decoded = jwt.decode(token, process.env.APPLICATION_SECRET);
        User.findOne({
            email: decoded.email
        }, function (err, user) {
            if (err) throw err;

            if (!user) {
                return next({code: 403, success: false, message: 'Authentication failed. User not found.'});
            } else {
                next(user);
            }
        })
    } else {
        return next({code: 403, success: false, message: 'No token provided.'});
    }
};
