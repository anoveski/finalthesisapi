var PrivateUser = require('../../models/private-user-model/private-user');
var jwt = require('jwt-simple');

module.exports = function (userCredentials, next) {
    PrivateUser.findOne({
        email: userCredentials.email
    }, function (err, privateUser) {
        if (err) throw err;

        if (!privateUser) {
            return next({
                code: 400, response: {
                    success: false, errorLocation: {
                        emailError: true,
                        passwordError: false
                    }, message: 'User not found.'
                }
            });
        } else {
            // check if password matches
            privateUser.comparePassword(userCredentials.password, function (err, isMatch) {
                if (isMatch && !err) {
                    // if user is found and password is right create a token

                    var token = jwt.encode(privateUser, process.env.APPLICATION_SECRET);

                    // return the information including token as JSON
                    return next({
                        code: 200,
                        response: {
                            success: true, token: 'Bearer ' + token, role: privateUser.role,
                            userData: {
                                firstName: privateUser.firstName,
                                lastName: privateUser.lastName,
                                email: privateUser.email
                            }
                        }
                    });
                } else {
                    return next({
                        code: 406, response: {
                            success: false,
                            errorLocation: {
                                emailError: false,
                                passwordError: true
                            }, message: 'Wrong password.'
                        }
                    });
                }
            });
        }
    });
};
