var stripeSecretKey = process.env.STRIPE_SECRET_KEY;
var stripe = require('stripe')(stripeSecretKey);
var generator = require('generate-serial-number');
var Ticket = require('../../models/ticket-model/ticket');
var PurchasedTicket = require('../../models/purchased-ticket-model/purchased-ticket');
var sendEmail = require('../../controllers/send-email/send-email');
var updateDatabaseCollection = require('../../controllers/update-database-collection/update-database-collection');
var path = require('path');
var hogan = require('hogan.js');
var fs = require('fs');

var filePath = path.join(process.cwd(), 'templates/purchased-ticket-email-body/purchased-ticket-email-body.hbs');
var template = fs.readFileSync(filePath, 'utf-8');
var compiledTemplate = hogan.compile(template);

module.exports = function (requestData, user, next) {

    Ticket.findOne({
        _id: requestData.ticketId
    }, function (err, ticket) {
        if (err) throw err;
        if (!ticket) {
            return next({code: 400, response: {success: false, message: 'Ticket not found'}})
        } else {
            var ticketPrice = 0;
            if (requestData.ticketType === 'oneWayTicket') {
                ticketPrice = ticket.oneWayPrice * 100;

                if (requestData.selectedDeparture.startLocationDate !== '' &&
                    requestData.selectedDeparture.startLocationHour.departureTime) {
                    chargeCustomerAndReserveTicket(requestData, ticket, ticketPrice, user, function (status) {
                        if (status.success === true) {
                            purchasedTicketSuccessHandler(status, user, function (response) {
                                return next(response);
                            })
                        }
                    })
                } else {
                    return next({
                        code: 500,
                        response: {success: false, message: 'Please enter full credentials'}
                    })
                }

            } else if (requestData.ticketType === 'returnTicket') {
                ticketPrice = ticket.returnTicketPrice * 100;
                if (requestData.selectedDeparture.startLocationDate !== '' &&
                    requestData.selectedDeparture.startLocationHour.departureTime &&
                    requestData.selectedDeparture.returnLocationDate !== '' &&
                    requestData.selectedDeparture.returnLocationHour.departureTime) {
                    chargeCustomerAndReserveTicket(requestData, ticket, ticketPrice, user, function (status) {

                        if (status.success === true) {
                            purchasedTicketSuccessHandler(status, user, function (response) {
                                return next(response);
                            })
                        }
                    })
                } else {
                    return next({
                        code: 500,
                        response: {success: false, message: 'Please enter full credentials'}
                    })
                }
            }

        }
    })
};

function chargeCustomerAndReserveTicket(requestData, ticket, ticketPrice, user, next) {
    stripe.charges.create({
        amount: ticketPrice,
        source: requestData.stripeUserPurchaseTicket,
        currency: 'mkd'
    }).then(function () {

        var newPurchasedTicket = PurchasedTicket({
            purchasedTicketNo: generator.generate(7),
            companyId: ticket.companyId,
            companyNo: ticket.companyNo,
            companyName: ticket.companyName,
            startLocation: ticket.startLocation,
            destinationLocation: ticket.destinationLocation,
            customerId: user._id,
            activePurchasedTicketStatus: true,
            todayActivePurchasedTicketStatus: false,
            price: ticketPrice / 100,
            ticketId: ticket._id,
            ticketType: requestData.ticketType,
            passenger: requestData.passenger,
            selectedDeparture: requestData.selectedDeparture,
            expireDate: extractTheExpireDate(requestData.selectedDeparture)
        });

        newPurchasedTicket.save(function (err) {
            if (err) {
                return next({
                    code: 500,
                    response: {success: false, message: 'Ticket not successfully purchased'}
                })
            } else {
                updateDatabaseCollection(ticket, {
                    availableSeats: ticket.availableSeats - 1
                }, function () {
                    return next({code: 202, success: true, purchasedTicketData: newPurchasedTicket})
                });
            }
        });
    }).catch(function (error) {
        next({
            code: 203, response: {
                success: false,
                message: 'Card error. Your card is invalid or you may haven`t got enough credit'
            }
        })
    });
}

function extractTheExpireDate(departureTime) {
    if (departureTime.returnLocationDate !== '') {
        return departureTime.returnLocationDate
    } else {
        if (departureTime.startLocationDate !== '') {
            return departureTime.startLocationDate
        }
    }
}

function purchasedTicketSuccessHandler(status, user, next) {
    sendEmail({
            recipientEmail: user.email,
            messageSubject: 'Final thesis purchased ticket',
            messageBody: compiledTemplate.render()
        }, {ticketData: status.purchasedTicketData},
        'purchased-ticket-template/purchased-ticket',
        function (response) {
            if (response === true) {
                return next({
                    code: status.code,
                    response: {
                        success: status.success,
                        purchasedTicketData: status.purchasedTicketData,
                        ticketSendOnEmail: true
                    }
                });
            } else if (response === false) {
                return next({
                    code: status.code,
                    response: {
                        success: status.success,
                        purchasedTicketData: status.purchasedTicketData,
                        ticketSendOnEmail: false
                    }
                });
            }
        })
}
