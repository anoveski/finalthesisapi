var socketIo = require('socket.io');

var latitude = 41.1829852;
var longitude = 20.6731971;

module.exports = function (server) {
    var io = socketIo(server);

    io.on('connection', function (socket) {
        socket.on('location', function (data) {
            latitude = data.latitude;
            longitude = data.longitude;
        });

        setInterval(function () {
            socket.emit('vehicleLocation', {lat: latitude, lon: longitude})
        }, 1000)
    });
};
