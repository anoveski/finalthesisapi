var express = require('express');
var router = express.Router();
var passport = require('passport');
var jwt = require('jwt-simple');
var rimraf = require('rimraf');
var getToken = require('../../controllers/get-token/get-token');
var PurchasedTicket = require('../../models/purchased-ticket-model/purchased-ticket');
var generatePdf = require('../../controllers/generate-pdf-file/generate-pdf');

router.post('/downloadTicket', passport.authenticate('jwt', {session: false}), (req, res) => {
    var token = getToken(req.headers);
    var decoded = jwt.decode(token, process.env.APPLICATION_SECRET);

    var condition;

    if (decoded.role === 'PRIVATE_USER') {
        condition = {
            customerId: decoded._id,
            _id: req.body.purchasedTicketId
        };
    } else if (decoded.role === 'BUSINESS_USER') {
        condition = {
            companyId: decoded.companySerialNo,
            _id: req.body.purchasedTicketId
        };
    }

    PurchasedTicket.findOne(condition, function (err, ticket) {
        if (err) throw err;

        if (ticket) {
            generatePdf({ticketData: ticket}, 'purchased-ticket-template/purchased-ticket',
                function (data) {
                    res.status(200).download(data.dirPath + '/' + data.fileName, data.fileName,
                        function (response) {
                            removeDirectory(data.dirPath);
                        })
                })
        }
    });

    var removeDirectory = function (path) {
        rimraf.sync(path);
    };
});

module.exports = router;
