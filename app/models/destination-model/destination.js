var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DestinationSchema = new Schema({
    townOrMunicipality: {
        type: String,
        required: true
    },
    country: {
        name: {
            type: String,
            required: true
        },
        code: {
            type: String,
            required: true
        }
    }
});

module.exports = function (collection, next) {
    return next(mongoose.model(collection, DestinationSchema))
};
