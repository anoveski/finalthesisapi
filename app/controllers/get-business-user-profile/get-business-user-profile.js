var BusinessUser = require('../../models/business-user-model/business-user');
var jwt = require('jwt-simple');

module.exports = function (token, next) {
    if (token) {
        var decoded = jwt.decode(token, process.env.APPLICATION_SECRET);
        BusinessUser.findOne({
            email: decoded.email
        }, function (err, user) {
            if (err) throw err;

            if (!user) {
                return next({code: 403, success: false, message: 'Authentication failed. User not found.'});
            } else {
                next({
                    _id: user._id,
                    role: user.role,
                    companyNo: user.companyNo,
                    companyName: user.companyName,
                    country: user.country,
                    companyPhoneNumber: user.companyPhoneNumber,
                    email: user.email
                });
            }
        })
    } else {
        return next({code: 403, success: false, message: 'No token provided.'});
    }
};
