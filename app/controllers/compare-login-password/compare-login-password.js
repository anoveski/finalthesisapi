var bcrypt = require('bcrypt');

module.exports = function (pass, password, result) {
    bcrypt.compare(pass, password, function (err, isMatch) {
        if (err) {
            return result(err);
        }
        return result(null, isMatch);
    });
};
