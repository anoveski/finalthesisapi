var express = require('express');
var app = express();
var port = process.env.PORT || 8080;
var fs = require('fs');
var http = require('http');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');

var passport = require('passport');

if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config()
}

var dbConnectionString = process.env.DB_CONNECTION_STRING;

app.use(passport.initialize());

mongoose.connect(dbConnectionString, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true
});

require('./config/passport/passport')(passport);

var registerRouter = require('./app/routes/register-account/register-account');
var loginRouter = require('./app/routes/login/login');
var addTicket = require('./app/routes/add-tickets/add-tickets');
var getCompanyTickets = require('./app/routes/get-company-tickets/get-company-tickets');
var getProfile = require('./app/routes/my-profile/my-profile');
var getDestinations = require('./app/routes/get-destinations/get-destinations');
var browseDestinations = require('./app/routes/browse-destinations/browse-destinations');
var purchaseTicket = require('./app/routes/purchase-ticket/purchase-ticket');
var getPurchaseTicket = require('./app/routes/get-purchased-tickets/get-purchased-tickets');
var downloadTicket = require('./app/routes/download-ticket/download-ticket');
var getDescriptionCardsData = require('./app/routes/app-data-routes/get-description-cards-data/get-description-cards-data');

var apiRoutes = express.Router();

app.use('/public', express.static('public'));

app.use(function (req, res, next) {
    var allowedOrigins = process.env.ALLOWED_ORIGINS.split(',');
    var origin = req.headers.origin;
    if (allowedOrigins.indexOf(origin) > -1) {
        res.setHeader('Access-Control-Allow-Origin', origin);
    }
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, PropertyType');
    res.header('Access-Control-Expose-Headers', 'Content-Disposition');
    next();
});

// get our request parameters
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.get('/', function (request, response) {
    response.set('Content-Type', 'text/html');

    fs.readFile('./app/views/home.html', null, function (error, data) {
        if (error) {
            response.send(404);
            response.send('Not found!')
        } else {
            response.send(data);
        }
    })
});

apiRoutes.use(
    registerRouter,
    loginRouter,
    addTicket,
    getCompanyTickets,
    getProfile,
    getDestinations,
    browseDestinations,
    purchaseTicket,
    getPurchaseTicket,
    downloadTicket,
    getDescriptionCardsData
);

app.use('/api', apiRoutes);

var server = http.Server(app);
server.listen(port);
console.log('The server is up and ready on: http://localhost:' + port);

require('./app/controllers/vehicle-tracker/vehicle-tracker')(server);

