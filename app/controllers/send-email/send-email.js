var gmailMailerConfig = require('../../../config/gmail-mailer/gmail-mailer');
var rimraf = require('rimraf');
var generatePdf = require('../../controllers/generate-pdf-file/generate-pdf');

module.exports = function (messageData, pdfDate, pdfTemplate, next) {

    generatePdf(pdfDate, pdfTemplate, function (data) {
        const smtpTransport = gmailMailerConfig();

        const mailOptions = {
            from: process.env.GMAIL_ACCOUNT,
            to: messageData.recipientEmail,
            subject: messageData.messageSubject,
            attachments: [{
                filename: data.fileName,
                path: data.dirPath + '/' + data.fileName,
                contentType: 'application/pdf'
            }],
            generateTextFromHTML: true,
            html: messageData.messageBody
        };

        smtpTransport.sendMail(mailOptions, (error, response) => {
            removeDirectory(data.dirPath);
            if(response){
                return next(true)
            }
            if(error){
                return next(false)
            }
            smtpTransport.close();
        });
    }); // generate PDF

    var removeDirectory = function (path) {
        rimraf.sync(path);
    };
};
