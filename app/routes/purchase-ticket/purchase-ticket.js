var express = require('express');
var router = express.Router();
var passport = require('passport');
var purchaseTicket = require('../../controllers/purchase-ticket/purchase-ticket');
var jwt = require('jwt-simple');
var getToken = require('../../controllers/get-token/get-token');

router.post('/purchaseTicket', passport.authenticate('jwt', {session: false}), (req, res) => {
    var token = getToken(req.headers);

    var userProfile = jwt.decode(token, process.env.APPLICATION_SECRET);

    purchaseTicket(req.body, userProfile, function (status) {
        if (status.response.success === true) {
            res.status(status.code).json(status.response);

        } else if (status.response.success === false) {
            res.status(status.code).json(status.response);
        }
    });
});

module.exports = router;
