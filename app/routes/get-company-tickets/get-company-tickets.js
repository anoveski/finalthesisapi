var express = require('express');
var router = express.Router();
var passport = require('passport');
var jwt = require('jwt-simple');
var getToken = require('../../controllers/get-token/get-token');
var Ticket = require('../../models/ticket-model/ticket');

router.post('/getCompanyTickets', passport.authenticate('jwt', {session: false}), (req, res) => {
    var token = getToken(req.headers);
    var user = jwt.decode(token, process.env.APPLICATION_SECRET);

    var condition;
    if (req.body.ticketType === 'all') {
        condition = {
            companyId: user._id,
        }
    } else if (req.body.ticketType === 'active') {
        condition = {
            companyId: user._id,
            $or: [{
                expireDate: {$gte: new Date().toISOString()}
            }, {
                expireDate: '0'
            }]
        }
    } else if (req.body.ticketType === 'inactive') {
        condition = {
            companyId: user._id,
            $and: [{
                expireDate: {$lt: new Date().toISOString()}
            }, {
                expireDate: {$ne: '0'}
            }]
        }
    } else if (req.body.ticketType === 'weekly') {
        condition = {
            companyId: user._id,
            typeOfRelation: 'weekly',
            $or: [{
                expireDate: {$gte: new Date().toISOString()}
            }, {
                expireDate: '0'
            }]
        }
    } else if (req.body.ticketType === 'select_days') {
        condition = {
            companyId: user._id,
            typeOfRelation: 'select_days',
            $or: [{
                expireDate: {$gte: new Date().toISOString()}
            }, {
                expireDate: '0'
            }]
        }
    } else if (req.body.ticketType === 'every_day') {
        condition = {
            companyId: user._id,
            typeOfRelation: 'every_day'
        }
    }

    Ticket.find(condition, function (err, tickets) {
        if (err) throw err;
        else {
            res.status(200).json(tickets);
        }
    });

});

module.exports = router;
