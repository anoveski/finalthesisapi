var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var encryptPass = require('../../controllers/encrypt-password/encrypt-password');
var compareLoginPass = require('../../controllers/compare-login-password/compare-login-password');

var BusinessUserSchema = new Schema({
    role: {
        type: String,
        required: true
    },
    companyNo: {
        type: Number,
        required: true
    },
    companyName: {
        type: String,
        required: true
    },
    country: {
        name: {
            type: String,
            required: true
        },
        code: {
            type: String,
            required: true
        },
        type: Object,
        required: true
    },
    companyPhoneNumber: {
        type: String,
        required: true
    }, email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    }
});

BusinessUserSchema.pre('save', function (next) {
    var user = this;
    if (this.isModified('password') || this.isNew) {
        encryptPass(user, function () {
            next();
        })
    } else {
        return next();
    }
});

BusinessUserSchema.methods.comparePassword = function (pass, cb) {
    compareLoginPass(pass, this.password, function (error, result) {
        cb(error, result);
    });
};

module.exports = mongoose.model('BusinessUser', BusinessUserSchema);
