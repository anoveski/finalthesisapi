var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TicketSchema = new Schema({
    companyNo: {
        type: Number,
        required: true
    },
    companyId: {
        type: String,
        required: true
    },
    companyName: {
        type: String,
        required: true
    },
    companyPhoneNumber: {
        type: String,
        required: true
    },
    ticketSerialNo: {
        type: Number,
        required: true
    },
    validityStatus: {
        type: Number,
        required: true
    },
    startLocation: {
        townOrMunicipality: {
            type: String,
            required: true
        },
        country: {
            name: {
                type: String,
                required: true
            },
            code: {
                type: String,
                required: true
            }
        },
        type: Object,
        required: true
    },
    destinationLocation: {
        endTownOrMunicipality: {
            type: String,
            required: true
        },
        country: {
            name: {
                type: String,
                required: true
            },
            code: {
                type: String,
                required: true
            }
        },
        type: Object,
        required: true
    },
    typeOfTransportationVehicle: {
        type: String,
        required: true
    },
    availableSeats: {
        type: Number,
        required: true
    },
    oneWayPrice: {
        type: Number,
        required: true
    },
    returnTicketPrice: {
        type: Number
    },
    oneWayTicket: {
        type: Boolean,
        required: true
    },
    returnTicket: {
        type: Boolean,
        required: true
    },
    typeOfRelation: {
        type: String,
        required: true
    },
    relationData: {
        weeklyRelation: {
            repeatEveryWeek: {
                type: Boolean,
                required: true
            },
            selectedWeek: [{
                addEndDate: {
                    type: Boolean,
                    required: true
                },
                startDate: {
                    type: String,
                    required: true
                },
                endDate: {
                    type: String,
                    required: true
                },
                departureHoursStart: [{
                    departureTime: {
                        type: String,
                        required: true
                    }
                }],
                departureHoursDestination: [{
                    departureTime: {
                        type: String,
                        required: true
                    }
                }]
            }]
        },
        dailyRelation: [{
            dayDate: {
                type: String,
                required: true
            },
            departureHoursStart: [{
                departureTime: {
                    type: String,
                    required: true
                }
            }],
            departureHoursDestination: [{
                departureTime: {
                    type: String,
                    required: true
                }
            }]
        }],
        everyDayRelation: {
            departureHoursStart: [{
                departureTime: {
                    type: String,
                    required: true
                }
            }],
            departureHoursDestination: [{
                departureTime: {
                    type: String,
                    required: true
                }
            }]
        },
        type: Object,
        required: true
    },
    expireDate: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Ticket', TicketSchema);
