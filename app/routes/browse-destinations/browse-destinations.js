var express = require('express');
var router = express.Router();
var Ticket = require('../../models/ticket-model/ticket');

router.post('/browseDestinations', (req, res) => {

    var condition;

    if (req.body.startLocation !== undefined) {
        condition = {
            'startLocation.townOrMunicipality': req.body.startLocation.townOrMunicipality,
            'startLocation.country.code': req.body.startLocation.country.code,
            'destinationLocation.townOrMunicipality': req.body.destinationLocation.townOrMunicipality,
            'destinationLocation.country.code': req.body.destinationLocation.country.code,

            '$or': [{
                'expireDate': {'$gte': new Date().toISOString()}
            }, {
                'expireDate': '0'
            }]
        }
    } else {
        condition = {
            'startLocation.townOrMunicipality': req.body.townOrMunicipality,
            'startLocation.country.code': req.body.countryCode,

            '$or': [{
                'expireDate': {'$gte': new Date().toISOString()}
            }, {
                'expireDate': '0'
            }]
        }
    }

    Ticket.find(condition, function (err, tickets) {
        if (err) throw err;

        if (tickets) {
            res.status(200).json(tickets)
        }
    })

});

module.exports = router;
