var BusinessUser = require('../../models/business-user-model/business-user');
var jwt = require('jwt-simple');

module.exports = function (userCredentials, next) {
    BusinessUser.findOne({
        email: userCredentials.email
    }, function (err, businessUser) {
        if (err) throw err;

        if (!businessUser) {
            return next({
                code: 400, response: {
                    success: false, errorLocation: {
                        emailError: true,
                        passwordError: false
                    }, message: 'User not found.'
                }
            });
        } else {
            // check if password matches
            businessUser.comparePassword(userCredentials.password, function (err, isMatch) {
                if (isMatch && !err) {
                    // if user is found and password is right create a token

                    var token = jwt.encode(businessUser, process.env.APPLICATION_SECRET);

                    // return the information including token as JSON
                    return next({
                        code: 200,
                        response: {success: true, token: 'Bearer ' + token, role: businessUser.role}
                    });
                } else {
                    return next({
                        code: 406, response: {
                            success: false,
                            errorLocation: {
                                emailError: false,
                                passwordError: true
                            }, message: 'Wrong password.'
                        }
                    });
                }
            });
        }
    });
};
