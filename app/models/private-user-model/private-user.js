var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var encryptPass = require('../../controllers/encrypt-password/encrypt-password');
var compareLoginPass = require('../../controllers/compare-login-password/compare-login-password');

var PrivateUserSchema = new Schema({
    role: {
        type: String,
        required: true
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        required: true
    },
    date_of_birth: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    }
});

PrivateUserSchema.pre('save', function (next) {
    var user = this;
    if (this.isModified('password') || this.isNew) {
        encryptPass(user, function () {
            next();
        })

    } else {
        return next();
    }
});

PrivateUserSchema.methods.comparePassword = function (pass, cb) {
    compareLoginPass(pass, this.password, function (error, result) {
        cb(error, result);
    });
};

module.exports = mongoose.model('PrivateUser', PrivateUserSchema);
