var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DescriptionCardDataSchema = new Schema({
    icon: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    buttonTitle: {
        type: String,
        required: true
    },
});

module.exports = mongoose.model('DescriptionCard', DescriptionCardDataSchema);
