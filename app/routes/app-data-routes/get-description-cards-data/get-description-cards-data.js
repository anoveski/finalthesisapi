var express = require('express');
var router = express.Router();
var DescriptionCardsData = require('../../../models/get-description-cards-data-model/get-description-cards-data');

router.get('/applicationData/getDescriptionCardsData', (req, res) => {

    DescriptionCardsData.find({}, function (err, data) {
        if (err) throw err;
        if (data) {
            res.status(200).json(data);
        }
    })
});

module.exports = router;
