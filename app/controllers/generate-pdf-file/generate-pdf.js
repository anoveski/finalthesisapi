var path = require('path');
var fse = require('fs-extra');
var hbs = require('handlebars');
var tmp = require('tmp');
var date = require('date-and-time');
var puppeteer = require('puppeteer');
var JsBarcode = require('jsbarcode');
var {createCanvas} = require('canvas');

var compile = async function (templateName, data) {
    var filePath = path.join(process.cwd(), 'templates', `${templateName}.hbs`);
    var html = await fse.readFile(filePath, 'utf-8');
    return hbs.compile(html)(data);
};

var createTmpDirectory = function (next) {
    tmp.dir({template: process.cwd() + '/tmp/tmp-XXXXXX'},
        function _tempDirCreated(err, path) {
            if (err) throw err;
            return next(path);
        });
};

hbs.registerHelper('date', function (value, format) {
    return date.format(new Date(value), format);
});

hbs.registerHelper('ticketType', function (value) {
    if (value === 'oneWayTicket') {
        return 'One way';
    } else if (value === 'returnTicket') {
        return 'Return';
    }
});

hbs.registerHelper('showOrHideElement', function (mode, options) {
    return mode === 'returnTicket' ? options.fn(this) : '';
});

hbs.registerHelper('myBarcode', function (value) {
    var canvas = createCanvas(null, null, 'svg');
    JsBarcode(canvas, value.toString(), {
        height: 30,
        displayValue: true,
        font: "fantasy"
    });
    return canvas.toDataURL();
});


module.exports = async function (data, templateName, next) {

    (async () => {
        const browser = await puppeteer.connect({
            browserWSEndpoint: 'wss://chrome.browserless.io/'
        });
        const page = await browser.newPage();

        var content = await compile(templateName, data);

        createTmpDirectory(async function (directoryPath) {
            var fileName = data.ticketData.companyName + '_' + data.ticketData.startLocation.townOrMunicipality +
                ' to ' + data.ticketData.destinationLocation.townOrMunicipality + '_' +
                data.ticketData.passenger.firstName + ' ' + data.ticketData.passenger.lastName + '.pdf';
            await page.setContent(content);
            await page.emulateMedia('screen');
            await page.pdf({
                path: directoryPath + '/' + fileName,
                format: 'A4',
                printBackground: true
            });

            await browser.close();
            return next({dirPath: directoryPath, fileName: fileName})
        });
    })();
};
