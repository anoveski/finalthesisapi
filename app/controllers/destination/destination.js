var DestinationModel = require('../../models/destination-model/destination');

module.exports = function (collection, potentialNewDestination, next) {
    DestinationModel(collection, function (locationSchema) {
        locationSchema.findOne({
            townOrMunicipality: potentialNewDestination.townOrMunicipality
        }, function (err, result) {
            if (err) throw err;

            if (!result) {
                DestinationModel(collection, function (locationSchema) {
                    var destination = new locationSchema({
                        townOrMunicipality: potentialNewDestination.townOrMunicipality,
                        country: {
                            name: potentialNewDestination.country.name,
                            code: potentialNewDestination.country.code,
                        }
                    });
                    destination.save(function (err) {
                        if (err) {
                            return next(err)
                        } else {
                           return next({newDestination: true})
                        }
                    })
                });

            } else {
                 return next({newDestination: false})
            }
        })
    })
};
