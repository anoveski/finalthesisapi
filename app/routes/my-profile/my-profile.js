var express = require('express');
var router = express.Router();
var passport = require('passport');
var jwt = require('jwt-simple');
var getToken = require('../../controllers/get-token/get-token');

router.get('/getMyProfileData', passport.authenticate('jwt', {session: false}), (req, res) => {
    var token = getToken(req.headers);

    var userProfile = jwt.decode(token, process.env.APPLICATION_SECRET);

    if (userProfile.role === 'PRIVATE_USER') {
        res.status(200).json({
            _id: userProfile._id,
            role: userProfile.role,
            firstName: userProfile.firstName,
            lastName: userProfile.lastName,
            gender: userProfile.gender,
            date_of_birth: userProfile.date_of_birth,
            email: userProfile.email,
        });
    } else if (userProfile.role === 'BUSINESS_USER') {
        res.status(200).json({
            _id: userProfile._id,
            role: userProfile.role,
            companyNo: userProfile.companyNo,
            companyName: userProfile.companyName,
            country: userProfile.country,
            companyPhoneNumber: userProfile.companyPhoneNumber,
            email: userProfile.email
        });
    }
});

module.exports = router;
