var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    email: {
        type: String,
        unique: true,
        required: true
    },
    role: {
        type: String,
        required: true
    }
});

UserSchema.pre('save', function (next) {
    next();
});

module.exports = mongoose.model('User', UserSchema);
