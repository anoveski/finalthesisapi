var express = require('express');
var router = express.Router();
var generator = require('generate-serial-number');
var User = require('../../models/user-module/user');
var PrivateUser = require('../../models/private-user-model/private-user');
var BusinessUser = require('../../models/business-user-model/business-user');

router.post('/register', (req, res) => {
    if (req.body.role === 'BUSINESS_USER') {
        if (!req.body.companyName || !req.body.email || !req.body.password) {
            res.json({success: false, message: 'Please enter valid credentials'});
        } else {
            var newBusinessUser = new BusinessUser({
                role: req.body.role,
                companyNo: generator.generate(10),
                companyName: req.body.companyName,
                country: {
                    name: req.body.country.name,
                    code: req.body.country.code
                },
                companyPhoneNumber: req.body.companyPhoneNumber,
                email: req.body.email,
                password: req.body.password
            });
            newBusinessUser.save(function (err) {
                if (err) {
                    res.json({success: false, message: 'Not registered! Email already exists'})
                } else {
                    var newUser = new User({
                        email: req.body.email,
                        role: req.body.role,
                    });
                    newUser.save(function (err) {
                        if (err) {
                            res.status(500).json({success: false, message: 'Not fully registered'})
                        } else {
                            res.status(201).json({success: true, message: 'Successful created user!'})
                        }
                    });
                }
            })
        }

    } else if (req.body.role === 'PRIVATE_USER') {
        if (!req.body.firstName || !req.body.lastName ||
            !req.body.gender || !req.body.date_of_birth ||
            !req.body.email || !req.body.password) {
            res.status(417).json({success: false, message: 'Please enter valid credentials'});
        } else {
            var newPrivateUser = new PrivateUser({
                role: req.body.role,
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                gender: req.body.gender,
                date_of_birth: req.body.date_of_birth,
                email: req.body.email,
                password: req.body.password
            });
            newPrivateUser.save(function (err) {
                if (err) {
                    res.status(409).json({success: false, message: 'Not registered! Email already exists'})
                } else {
                    var newUser = new User({
                        email: req.body.email,
                        role: req.body.role,
                    });
                    newUser.save(function (err) {
                        if (err) {
                            res.status(500).json({success: false, message: 'Not fully registered'})
                        } else {
                            res.status(201).json({success: true, message: 'Successful created user!'})
                        }
                    });
                }
            })
        }
    }
});

module.exports = router;
