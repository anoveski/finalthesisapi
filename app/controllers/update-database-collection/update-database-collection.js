var Ticket = require('../../models/ticket-model/ticket');

module.exports = function (data, updatedData, next) {
    Ticket.updateOne({'_id': data._id}, {$set: updatedData},
        function (err, result) {
            if (err) throw err;

            if (result) {
                return next();
            } else {
                return next();
            }
        })
};
